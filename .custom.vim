
" Для быстрого сохранения сессии
nnoremap <F12> :tabdo NERDTreeClose<CR>:tabdo TagbarClose<CR>:mksession! Session.vim<CR>:tabdo Tagbar<CR>:tabdo NERDTree<CR>:tabdo wincmd p<CR>
"nnoremap <F12> :NERDTreeClose 
" Отрыть последнюю сессию 
source Session.vim

" Сразу запустить NERDTree и Tagbar
autocmd VimEnter * tabdo Tagbar
autocmd VimEnter * tabdo NERDTree | wincmd p
"autocmd VimEnter * NERDTree
"autocmd BufEnter * NERDTreeMirrir
"autocmd VimEnter * w wincmd
