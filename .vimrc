" this confis saved in bitbusket under git
" make soft link for with repository
"       $ln -s ~/.vim/.vimrc ~/.vimrc
" for get config from bitbusket
"       $cd ~/.vim
"       $git clone https://griphus@bitbucket.org/griphus/vimrc.git
"       $cp ../.vimrc ../.vimrc_copy
"       #ln .vimrc ../.vimrc
" for save changes in bitbusket:
"       $cd ~/.vim
"       $git push origin master


filetype on
filetype plugin on

"set fileencodings=cp1251,utf8,koi8r,cp866,ucs-2le
set termencoding=utf8
set fileencodings=utf8,cp1251
set encoding=utf8
"set guifont=Monospace\ 8
"set guifont=DejaVu\ Sans\ Mono\ 8
set guifont=Courier\ New\ 10
"set guifont=Droid\ Sans\ Mono\ 10

set number
set smartindent

" Python specific settings
set tabstop=4
set shiftwidth=4
set smarttab
set expandtab
set softtabstop=4
set autoindent
let python_highlight_all=1
set t_Co=256

"Настройка omnicomletion для Python (а так же для js, html и css)
autocmd FileType python set omnifunc=pythoncomplete#Complete
autocmd FileType javascript set omnifunc=javascriptcomplete#CompleteJS
autocmd FileType html set omnifunc=htmlcomplete#CompleteTags
autocmd FileType css set omnifunc=csscomplete#CompleteCSS

"Авто комплит по табу
function InsertTabWrapper()
    let col = col('.') - 1
    if !col || getline('.')[col - 1] !~ '\k'
        return "\"
    else
        return "\<c-p>"
    endif
endfunction
imap <c-r>=InsertTabWrapper()"Показываем все полезные опции автокомплита сразу
set complete=""
set complete+=.
set complete+=k
set complete+=b
set complete+=t

"Перед сохранением вырезаем пробелы на концах (только в .py файлах)
autocmd BufWritePre *.py normal m`:%s/\s\+$//e ``
"В .py файлах включаем умные отступы после ключевых слов
autocmd BufRead *.py set smartindent cinwords=if,elif,else,for,while,try,except,finally,def,class

"Цветовая тема
syntax enable
if has('gui_running')
    "colorscheme kolor
    "colorscheme peachpuff 
    "set background=dark
    set background=light
    colorscheme solarized 
    "colorscheme peachpuff  
else
    colorscheme kolor
endif

"Настройка CtrlPI для умного поиска
"set runtimepath^=~/.vim/bundle/ctrlp.vim

"Для русской раскладки
"set langmap=ёйцукенгшщзхъфывапролджэячсмитьбю;`qwertyuiop[]asdfghjkl\;'zxcvbnm\,.,ЙЦУКЕHГШЩЗХЪФЫВАПРОЛДЖЭЯЧСМИТЬБЮ;QWERTYUIOP{}ASDFGHJKL:\"ZXCVBNM<>

" F2 - Run recursive grep
nnoremap <silent> <F2> :Rgrep<cr>
" Shift-F2 - Same as ":Rgrep" but adds the results to the current results
nnoremap <silent> <S-F2> :RgrepAdd<cr>
" F3 - Search for a pattern on all open buffers
nnoremap <silent> <F3> :GrepBuffer<cr>

" Открытый файл всегда будет рабочим каталогом
autocmd BufEnter * silent! lcd %:p:h

" Восстановить последнюю сессию
"mksession Session.vim

"для NERDCommenter изменения комментария на //
"let NERD_c_alt_style=1
"let NERD_cpp_alt_style=1
"let g:NERDCustomDelimiters = {'c': {'leftAlt': '//', 'rightAlt': '', 'left': '//', 'right': '//' }, }

if filereadable(".custom.vim")
    so .custom.vim
endif

    " Следующие два блока перенесены в файл .vim.custom, который должен лежать
    " в корне рабочего проекта и соот. настраивается для удобства проекта
" Для открытия сессии в файле Session.vim
nnoremap <F9> :source Session.vim<CR>:tabdo Tagbar<CR>:tabdo NERDTree<CR>:tabdo wincmd p<CR>
" Для сохранения сессии
nnoremap <F12> :tabdo NERDTreeClose<CR>:tabdo TagbarClose<CR>:mksession! Session.vim<CR>:tabdo Tagbar<CR>:tabdo NERDTree<CR>:tabdo wincmd p<CR>



